  general time;  
  aero torque;
  aero power;
  aero thrust;
  constraint bearing1 shaft_rot 2; angle and angle velocity 
  constraint bearing2 pitch1 5;    angle and angle velocity 
  constraint bearing2 pitch2 5;    angle and angle velocity 
  constraint bearing2 pitch3 5;    angle and angle velocity 
  wind free_wind 1 0.0 0.0 -119; local wind at fixed position: coo (1=global,2=non-rotation rotor coo.), pos x, pos y, pos z
  mbdy momentvec tower  1 1 tower # tower base ;
  mbdy forcevec  tower  1 1 tower # tower base ;
  mbdy momentvec towertop 1 1 towertop # yaw bearing ;
  mbdy forcevec  towertop 1 1 towertop # yaw bearing  ;
  mbdy forcevec blade1 1  1 blade1 # blade 1 root ;
  mbdy momentvec blade1 1  1 blade1 # blade 1 root ;
  mbdy forcevec blade2 1  1 blade2 # blade 2 root ;
  mbdy momentvec blade2 1  1 blade2 # blade 2 root ;
  mbdy forcevec blade3 1  1 blade3 # blade 3 root ;
  mbdy momentvec blade3 1  1 blade3 # blade 3 root ;
; Output from controller
  dll type2_dll dtu_we_controller inpvec  1  #  1: Generator torque reference               [Nm]         ;
  dll type2_dll dtu_we_controller inpvec  2  #  2: Pitch angle reference of blade 1         [rad]        ;
  dll type2_dll dtu_we_controller inpvec  3  #  3: Pitch angle reference of blade 2         [rad]        ;
  dll type2_dll dtu_we_controller inpvec  4  #  4: Pitch angle reference of blade 3         [rad]        ;
  dll type2_dll dtu_we_controller inpvec  5  #  5: Power reference                          [W]          ;
; ---------------------------------------------------------------------------------------                                                          
hydro water_surface 0.0 0.0 ;        x,y   gl. pos
; JACKET moments and forces                                                                                                               
mbdy momentvec F1 1 1 F1 # Foundation 1 moments ;                                                                                                     
mbdy forcevec  F1 1 1 F1 # Foundation 1 forces ;  
mbdy momentvec F2 1 1 F2 # Foundation 2 moments ;                                                                                                     
mbdy forcevec  F2 1 1 F2 # Foundation 2 forces ;
mbdy momentvec F3 1 1 F3 # Foundation 3 moments ;                                                                                                     
mbdy forcevec  F3 1 1 F3 # Foundation 3 forces ;
mbdy momentvec F4 1 1 F4 # Foundation 4 moments ;                                                                                                     
mbdy forcevec  F4 1 1 F4 # Foundation 4 forces ;
mbdy momentvec F1 1 1 global # Foundation 1 moments ;                                                                                                     
mbdy forcevec  F1 1 1 global # Foundation 1 forces ;  
mbdy momentvec F2 1 1 global # Foundation 2 moments ;                                                                                                     
mbdy forcevec  F2 1 1 global # Foundation 2 forces ;
mbdy momentvec F3 1 1 global # Foundation 3 moments ;                                                                                                     
mbdy forcevec  F3 1 1 global # Foundation 3 forces ;
mbdy momentvec F4 1 1 global # Foundation 4 moments ;                                                                                                     
mbdy forcevec  F4 1 1 global # Foundation 4 forces ;
;